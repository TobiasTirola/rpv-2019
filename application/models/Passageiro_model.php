<?php

class Passageiro_model extends CI_Model {

    public function buscar() {
        return $this->db->get('passageiro')->result_array();
    }

    public function novo($passageiro) {
        $this->db->insert('passageiro', $passageiro);
    }

    public function buscarPontos($cpf) {
        return $this->db->get_where("pontos", array("cpf" => $cpf))->row_array();
    }

    public function buscarDadosCliente($cpf){
        return $this->db->get_where("cliente", array("cpf" => $cpf))->row_array();
    }
}
