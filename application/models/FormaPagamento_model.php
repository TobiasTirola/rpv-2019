<?php

class FormaPagamento_model extends CI_Model {

    public function buscaTodos() {
        return $this->db->get("formapagamento")->result_array();
    }

    public function salva($formapagamento) {
        $this->db->insert("formapagamento", $formapagamento);
    }

}
