<?php




class trecho_model extends CI_Model
{
    public function buscaTodos()
    {
        return $this->db->get("trecho")->result_array();
    }

    public function buscaEspecifica($origem, $destino, $data)
    {
        $this->db->where("inicio", $origem);
        $this->db->where("fim", $destino);
        $trecho = $this->db->get("trecho")->result_array();
        return $trecho;
    }

    public function salva($trecho)
    {
        $this->db->insert("trecho", $trecho);
    }
    public function retornaPorLinha($linha)
    {
        return $this->db->get_where("trecho", array(
            "idLinha" => $linha
        ))->result_array();
    }
}
