<?php




class linha_model extends CI_Model
{
    public function buscaTodos()
    {
        return $this->db->get("linhaonibus")->result_array();
    }


    public function salva($linha)
    {
        $this->db->insert("linhaonibus", $linha);
    }
    public function retorna($id){
        return $this->db->get_where("linhaOnibus", array(
            "idLinhaOnibus" => $id
        ))->row_array();
    }
    public function salvar(){
        $id = $this->input->post('id');
        $linha = array(
            'nome' => $this->input->post('nome'),
            'itinerario' => $this->input->post('itinerario'),
            'localSaida' => $this->input->post('localSaida'),
            'localDestino' => $this->input->post('localDestino')
        );
        $this->db->where('id', $id);
        return $this->db->update('linhaOnibus', $linha);
    }
}
