<?php

class Parada_model extends CI_Model {

    public function buscaTodos() {
        return $this->db->get("parada")->result_array();
    }

    public function salva($parada) {
        $this->db->insert("parada", $parada);
    }

}
