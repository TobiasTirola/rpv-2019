<?php

class AlterarTarifa_model extends CI_Model {

    public function buscarLinha() {
        return $this->db->get('linha')->result_array();
    }

    public function buscarEmpresa() {
        return $this->db->get('empresa')->result_array();
    }

    public function novaTarifa($tarifa) {
        $this->db->insert('tarifa', $tarifa);
    }

}
