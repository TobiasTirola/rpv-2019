
<?php
class usuarios_model extends CI_Model{
    public function salva($usuario){
         $this->db->insert("usuarios", $usuario);
    }
    
    public function logarUsuarios($nome, $senha){
        $this->db->where("nome", $nome);
        $this->db->where("senha", $senha);
        $usuario = $this->db->get("usuarios")->row_array();
        return $usuario;
    }
}
