<?php

class Trajeto_model extends CI_Model {

    public function buscaTodos() {
        return $this->db->get("trajetoo")->result_array();
    }

    public function salva($trajeto) {
        print_r($trajeto);
        $this->db->insert("linha", $trajeto);
    }

    public function buscar($id) {
        return $this->db->get_where("linha", array("id" => $id))->row_array(); // retorna o dado comparando os id 
    }

}
