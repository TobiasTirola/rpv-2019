<?php

class Concessao_model extends CI_Model {

    public function buscar($id) {
        return $this->db->get_where("concessao", array("id_trajeto" => $id))->row_array(); // retorna o dado comparando os id 
    }

    public function update() {
        $id = $this->input->post('id_concessao');
        $concessao = array(
            'id_concessao' => $this->input->post('id_concessao'),
            'contrato' => $this->input->post('contrato'),
            'dataVigencia' => $this->input->post('data'),
            'status' => $this->input->post('status'),
            'id_trajeto' => $this->input->post('id_trajeto')
        );
        $this->db->where('id_concessao', $id);
        return $this->db->update('concessao', $concessao);
    }

    public function salvar($concessao) {
        $this->db->insert('concessao', $concessao);
    }

}
