


<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Cadastro de trechos</h1>

    </div>


    <div class="container">
        <form action="<?= base_url('trecho/novo')?>" method="post">
            <label>Local de inicio:</label><input type="text" id="inicio" name="inicio" class="form-control">
            <label>Local de fim:</label> <input type="text" id="fim" name="fim" class="form-control">
            <label>Distância em km:</label> <input type="text" id="distanciaLinha" name="distanciaLinha" onkeypress="return SomenteNumero(event)" class="form-control">
            <label>Trajeto:</label> <input type="text" id="trajeto" name="trajeto" class="form-control">
            <br><br>
            <button class="btn-success" type="submit" onclick="myFunction()">Cadastrar</button>
        </form>



    </div>
</div>
<script>
    function myFunction() {
        alert("Cadastrado com sucesso!");
    }
</script>

<!-- /.container-fluid -->

