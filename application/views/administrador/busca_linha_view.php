<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"></h1>
        <table class="table">
            <tr>
                <th>Nome da linha</th>
            </tr>
            <?php foreach ($linhas as $linha): ?>
                <tr>
                    <td><?= $linha['nome'] ?></td>
                    <td>
                        <form action="edita" method="post"><input type="hidden"
                                                                        value="<?php echo $linha['idLinhaOnibus'] ?>">
                            <button type="submit" class="btn-success">Editar</button>
                        </form>
                    </td>
                    <td>
                        <form action="linha/deleta" method="post"><input type="hidden"
                                                                         value="<?= $linha['idLinhaOnibus'] ?>">
                            <button type="submit" class="btn-danger">Deletar</button>
                        </form>
                    </td>
                </tr>
            <?php endforeach ?>
        </table>
    </div>


</div>
<!-- /.container-fluid -->


