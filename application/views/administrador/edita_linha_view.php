    <!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Edição de linha</h1>

    </div>


    <div class="container">
        <form action="<?php echo base_url('linha/edit') ?>" method="post">

            <label>Nome:</label> <input type="text" id="nome" name="nome" class="form-control" value="<?= $linha['nome'] ?>">
            <label>Itinerário:</label> <input type="text" id="itinerario" name="itinerario" class="form-control" value="<?= $linha['itinerario'] ?>">
            <label>Local de saída:</label><input type="text" id="localSaida" name="localSaida" class="form-control" value="<?= $linha['localSaida'] ?>">
            <label>Local de Destino:</label> <input type="text" id="localDestino" name="localDestino" value="<?= $linha['localDestino'] ?>"
                                                    class="form-control">


            <br><br> <label>Trechos:</label>
            <div>
                <ul>
                    <?php foreach ($trechos as $trecho): ?>
                        <li>
                            <label><input type="checkbox" id="tipoOnibus" name="tipoOnibus"
                                          value="<?= $trecho['idTrecho'] ?>[]"><?= $trecho['inicio'] ?>
                                - <?= $trecho['fim'] ?></label>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>
            <button class="btn-success" type="submit">Editar</button>
        </form>


    </div>
</div>

<!-- /.container-fluid -->

