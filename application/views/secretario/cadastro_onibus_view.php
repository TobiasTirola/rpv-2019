<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Cadastro de ônibus</h1>

    </div>


    <div class="container">
        <form action="Onibus/novo" method="post">
            <label>Data de fabricação:</label> <input type="date" id="dtfabricacao" name="dtfabricacao"
                                                      class="form-control">
            <label>Marca:</label> <input type="text" id="marca" name="marca" class="form-control">
            <label>Modelo:</label><input type="text" id="modelo" name="modelo" class="form-control">
            <label>Data de aquisição:</label> <input type="date" id="dtAqui" name="dtAqui" class="form-control">
            <label>Quilometragem:</label> <input type="text" id="quilometragem" name="quilometragem"
                                                 class="form-control" onkeypress="return SomenteNumero(event)">
            <label>Placa:</label><input type="text" id="placa" name="placa" class="form-control">
            <label>Renavan:</label><input type="text" id="renavan" name="renavan" class="form-control"
                                          onkeypress="return SomenteNumero(event)">
            <label>Cor:</label><input type="text" id="cor" name="cor" class="form-control">
            <label>Número de assentos:</label> <input type="text" id="nmrassentos" name="nmrassentos"
                                                      class="form-control" onkeypress="return SomenteNumero(event)">
            <label>Número máximo de passageiros:</label><input type="text" id="nmrmax" name="nmrmax"
                                                               class="form-control"
                                                               onkeypress="return SomenteNumero(event)">
            <label>Status:</label><input type="text" id="status" name="status" class="form-control">
            <label>Tipo de ônibus:</label>
            <div class="radio">
                <label><input type="radio" id="tipoOnibus" name="tipoOnibus" value="Urbano" checked>Urbano</label>
                <label><input type="radio" id="tipoOnibus" name="tipoOnibus" value="Interurbano">Interurbano</label>
            </div>
            <button class="btn btn-primary btn-user btn-block" type="submit">Cadastrar</button>
        </form>


    </div>
</div>

<!-- /.container-fluid -->

