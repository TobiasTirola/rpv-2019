<div class="container">
    <div class="jumbotron">
        <h2 >Editar concessão</h2>                      
        <?= form_open('concessoes/salva') ?>
        <input type="hidden" class="hidden" name="id_concessao" value="<?= $concessao['id_concessao'] ?>">
        <input type="hidden" class="hidden" name="id_trajeto" value="<?= $concessao['id_trajeto'] ?>">
        <div class="form-group">
            <label  for="nome" >Contrato</label>
            <input type="text" class="form-control" name="contrato" id="contrato" value="<?= $concessao['contrato'] ?>" required>
        </div>
        <div class="form-group">
            <label  for="nome" >Data vigência</label>
            <input type="text" class="form-control" name="data" id="data" value="<?= $concessao['dataVigencia'] ?>" required>
        </div>
        <div class="form-group">
            <label  for="status">Status</label>
            <select class="form-control" name="status" id="status" required>
                <option value="">Selecione uma opção</option>
                <option value="1">Ativo</option>
                <option value="0">Desativo</option>              
            </select>
        </div>
        <button class="btn btn-primary">Salvar alterações</button>
        <?= form_close(); ?>
    </div>
</div>
