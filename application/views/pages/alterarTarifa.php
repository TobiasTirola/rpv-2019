<session>
    <div class="container">
        <h1 class="descricao-contato">Linhas</h1>
        <table class="table">
            <tr>
                <th class="descricao-contato">ID</th>
                <th class="descricao-contato">Nome</th>
                <th class="descricao-contato">Cidade</th>
            </tr>

            <?php foreach ($linhas as $linha) : ?>
                <tr>
                    <td class="descricao-contato"><?= $linha['id'] ?></td>
                    <td class="descricao-contato"><?= $linha['nome'] ?> </td>
                    <td class="descricao-contato"><?= $linha['cidade'] ?></td>
                </tr>
            <?php endforeach ?>;

        </table>



        <div class="jumbotron" >
            <h2 class="descricao-contato">Alterar Tarifa</h2>
            <form action="Home/index" method="post">
                <div class="form-group">
                    <label class="descricao-contato" for="id">ID Linha</label>
                    <input type="text" name="id" id="id" class="form-control">
                </div>

                <div class="form-group">
                    <label class="descricao-contato" for="valor">Novo Valor</label>
                    <input type="text" name="valor" id="valor" class="form-control">
                </div>

                <div class="form-group">
                    <label class="descricao-contato" for="data">Data</label>
                    <input type="text" name="data" id="data" class="form-control">
                </div>

                <div class="form-group">
                    <label class="descricao-contato" for="lei">Lei/Decreto</label>
                    <input type="text" name="lei" id="lei" class="form-control">
                </div>

                <button type="submit" class="btn btn-danger">Alterar</button>
            </form>
        </div>
    </div>
</session>