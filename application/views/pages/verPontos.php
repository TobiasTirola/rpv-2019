<div class="container">
    <div class="jumbotron">
        <h3>Dados Cliente</h3>
        <label>Nome: <?php echo $cliente['nome'] ?></label>
        <br>
        <label>Pontos: <?php echo $pontos['pontos'] ?></label>
        <br>
        <?php if ($pontos['pontos'] != 0) : ?>
            <label>Validade: <?php echo $pontos['validade'] ?></label>
            <br>
            <?php $segments = array('pontos', 'resgatar', $cliente['cpf']); ?>
            <a class="btn btn-primary" href="<?php echo site_url($segments) ?>">Troque seus pontos</a>
        <?php else : ?>    
           <?php $segments = array('trajetosUrbanos'); ?>
            <a class="btn btn-primary" href="<?php echo site_url($segments) ?>">Comprar passagem</a>
        <?php endif; ?>
    </div>
</div>
