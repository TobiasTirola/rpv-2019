<session>
    <div class="container">

        <div class="jumbotron" >
            <h2 >Gerir concessões</h2>
            <?= form_open('concessoes/visualizar') ?>

            <div class="form-group">
                <label for="linha_id">Trajeto</label>
                <select class="form-control" name="linha_id" id="linha_id" required>
                    <option value="">Selecione uma opção</option>
                    <?php foreach ($linhas as $linha) : ?>
                        <option value="<?= $linha['id'] ?>"><?= $linha['nome'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <button class="btn btn-danger">Visualizar</button>
            <?= form_close(); ?>
        </div>
    </div>

</session>