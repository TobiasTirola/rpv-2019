<div class="container">
    <div class="jumbotron">
        <h3>Dados concessão</h3>
        <label>Contrato: <?php echo $concessao['contrato'] ?></label>
        <br>
        <label>Data de vigência: <?php echo $concessao['dataVigencia'] ?></label>
        <br>
        <label>Status: 
            <?php
            if ($concessao['status'] == 1) {
                echo "Ativo";
            } else {
                echo "Desativo";
            }
            ?>
        </label>
        <br>
        <?php if ($concessao['id_trajeto'] != 0)  :  ?>
            <?php $segments = array('concessoes', 'editar', $concessao['id_trajeto']); ?>
            <a class="btn btn-danger" href="<?php echo site_url($segments) ?>">Editar</a>
        <?php else :?>    
            <?php $segments = array('concessoes', 'cadastrarNova', $id); ?>
            <a class="btn btn-danger" href="<?php echo site_url($segments) ?>">Adicionar</a>
        <?php                endif;?>
    </div>
</div>
