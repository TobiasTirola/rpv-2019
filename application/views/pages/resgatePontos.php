<session>
    <div class="container">
        <div class="jumbotron">
        <h2>Benefícios disponiveis com o programa de pontos</h2>
        <label>Nome: <?php echo $cliente['nome'] ?></label>
        <br>
        <label>Pontos: <?php echo $pontos['pontos'] ?></label>
        <br>
        <label>Validade: <?php echo $pontos['validade'] ?></label>
        <br>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Benefício</th>
                    <th scope="col">Pontos necessários</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td scope="row"> 1 passagem para uma viagem de 500km</td>
                    <td scope="row"> 300 pontos</td>
                </tr>
                <tr>
                    <td scope="row"> 2 passagens para uma viagem de 500km</td>
                    <td scope="row"> 500 pontos</td>
                </tr>
            </tbody>
        </table>
        </div>
    </div>
</session>