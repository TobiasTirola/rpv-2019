        
<session>
    <div class="container">
        <br><br>
        <div class="jumbotron">
            <h2>Categorias cadastradas</h2>
            <table class="table">
                <tr>
                    <th class="descricao-contato">Nome categoria</th>
                    <th class="descricao-contato">Valor a ser cobrado</th>
                </tr>

                <?php foreach ($categorias as $categoria) : ?>
                    <tr>
                        <td class="descricao-contato"><?= $categoria['descricao'] ?></td>
                        <?php if ($categoria['valorCobrado'] == 2): ?>
                            <td class="descricao-contato">Integral</td>
                        <?php elseif ($categoria['valorCobrado'] == 1): ?>
                            <td class="descricao-contato">Metade</td>
                        <?php else : ?>
                            <td class="descricao-contato">Isento</td>
                        <?php endif ?>
                    </tr>
                <?php endforeach ?>

            </table>

        </div>
    </div>
</session>