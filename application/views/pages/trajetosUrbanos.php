<section>
    <div class="container">
        <br><br>
        <div class="jumbotron">
            <a class="btn btn-primary btn-lg" href="<?php echo site_url('trajetosUrbanos/gerirConcessoes') ?>" role="button">Gerir concessões</a>
            <br><br>
            <a class="btn btn-primary btn-lg" href="<?php echo site_url('trajetosUrbanos/gerirTrajetos') ?>" role="button">Gerir trajetos</a>
            <br><br>
            <a class="btn btn-primary btn-lg" href="<?php echo site_url('trajetosUrbanos/alocarfunc') ?>" role="button">Alocar motoristas e cobradores</a>
            <br><br>
            <a class="btn btn-primary btn-lg" href="<?php echo site_url('trajetosUrbanos/gerirPassageiros') ?>" role="button">Gerenciar isenções/benefícios</a>
            <br><br>
            <a class="btn btn-primary btn-lg" href="<?php echo site_url('trajetosUrbanos/formasPagamento') ?>" role="button">Formas de pagamento</a>
            <br><br>
            <a class="btn btn-primary btn-lg" href="<?php echo site_url('trajetosUrbanos/programaPontos') ?>" role="button">Programa de pontos</a>
        </div>
    </div>
</section>