        
<session>
    <div class="container">
        <br><br>
        <div class="jumbotron">
            <h2>Formas de pagamento cadastradas</h2>
            <table class="table">
                <tr>
                    <th >Forma de pagamento</th>
                    <th >Descrição</th>
                </tr>

                <?php foreach ($formas as $forma) : ?>
                    <tr>
                        <td><?= $forma['FORMAPAGAMENTO'] ?></td>
                        <td><?= $forma['DESCRICAO'] ?></td>
                    </tr>
                <?php endforeach ?>

            </table>

        </div>
    </div>
</session>