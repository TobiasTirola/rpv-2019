        
<session>
    <div class="container">
        
        <h1>Cadastro de Forma de Pagamento</h1> 
        <?= form_open('formaPagamento/novo') ?>
        <div class="form-group"> 
            <label for="formaPagamento">Forma de Pagamento:</label> 
            <input type="text" class="form-control" name="formapagamento" id="formapagamento"> 
        </div> 
        <div class="form-group"> 
            <label for="descricao">Descrição:</label> 
            <input type="text" class="form-control" name="descricao" id="descricao"> 
        </div> 

        <button class="btn btn-default">Cadastrar</button> 
        <?= form_close(); ?>

        <br><br>
        <a class="btn btn-danger btn-lg" href="<?php echo site_url('formaPagamento/ver') ?>" role="button">Ver cadastradas</a>
        <br><br>

    </div>
</session>