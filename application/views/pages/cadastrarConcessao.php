<div class="container">
    <div class="jumbotron">
        <h2>Adicionar concessão</h2>                    
        <?= form_open('concessoes/novo') ?>
        <input type="hidden" class="hidden" name="id_trajeto" value="<?= $trajeto['id'] ?>">
        <div class="form-group">
            <label  for="nome" >Contrato</label>
            <input type="text" class="form-control" name="contrato" id="contrato"  required>
        </div>
        <div class="form-group">
            <label  for="nome" >Data vigência</label>
            <input type="text" class="form-control" name="data" id="data"  required>
        </div>
        <div class="form-group">
            <label  for="status">Status</label>
            <select class="form-control" name="status" id="status" required>
                <option value="">Selecione uma opção</option>
                <option value="1">Ativo</option>
                <option value="0">Desativo</option>              
            </select>
        </div>
        <button class="btn btn-primary">Salvar alterações</button>
        <?= form_close(); ?>
    </div>
</div>
