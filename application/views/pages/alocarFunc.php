        
<session>
    <div class="container">
        <br><br>
        <div class="jumbotron">
            <h2>Alocar funcionários</h2>
            <?= form_open_multipart('alocacao/novo') ?>

            <div class="form-group">
                <label for="onibus_id">Ônibus</label>
                <select class="form-control" name="onibus_id" id="onibus_id" required>
                    <option value="">Selecione uma opção</option>
                    <?php foreach ($listaOnibus as $onibus) : ?>
                        <option value="<?= $onibus['IDOnibus'] ?>"><?= $onibus['Placa'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="form-group">
                <label for="nome" >Data</label>
                <input type="text" class="form-control" name="nome" id="nome" required>
            </div>

            <div class="form-group">
                <label for="nome" >Horário início</label>
                <input type="text" class="form-control" name="nome" id="nome" required>
            </div>

            <div class="form-group">
                <label for="nome" >Horário saída</label>
                <input type="text" class="form-control" name="nome" id="nome" required>
            </div>

            <div class="form-group">
                <label for="linha_id">Motorista</label>
                <select class="form-control" name="linha_id" id="linha_id" required>
                    <option value="">Selecione uma opção</option>
                    <?php foreach ($motoristas as $motorista) : ?>
                        <option value="<?= $motorista['IDMotorista'] ?>"><?= $motorista['NomeMotorista'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="form-group">
                <label for="linha_id">Cobrador</label>
                <select class="form-control" name="linha_id" id="linha_id" required>
                    <option value="">Selecione uma opção</option>
                    <?php foreach ($cobradores as $cobrador) : ?>
                        <option value="<?= $cobrador['IDCobrador'] ?>"><?= $cobrador['NomeCobrador'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>



            <button class="btn btn-danger">Salvar</button>

            <?= form_close(); ?>
        </div>
    </div>
</session>