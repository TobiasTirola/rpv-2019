<section class="herois">
    <div class="container">
        <br><br>
        <h1>Paradas</h1>
        <table class="table">
            <tr>
                <th>Id</th>
                <th>Local</th>
                <th>Referencia</th>
            </tr>
            <?php foreach ($parada as $parada) : ?>
                <tr>
                    <td><?= $parada['id'] ?></td>
                    <td><?= $parada['local'] ?></td>
                    <td><?= $parada['referencia'] ?></td>
                </tr>
            <?php endforeach ?>
        </table>
        <br>

        <h1>Adicionar Paradas</h1>
        <?php
        echo form_open($action = "paradacont/novo");
        echo form_label($label_text = 'Local:', $id = 'local');
        echo form_input(array(
            "name" => "local",
            "id" => "local",
            "class" => "form-control",
            "maxlength" => "255"
        ));
        echo form_label($label_text = 'Referência:', $id = 'referencia');
        echo form_input(array(
            "name" => "referencia",
            "id" => "referencia",
            "class" => "form-control",
            "maxlength" => "255"
        ));
        ?>
        <br>
        <input type="submit" value="Adicionar" name="add" href="paradacont/novo"/>
        <?php
        //            echo form_button(array(
        //                "class" => "btn btn-primary",
        //                "type" => "submit",
        //                "content" => "Adicionar"
        //            ));
        echo form_close();
        ?>

    </div>
</section>