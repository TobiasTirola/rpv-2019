<div class="container">
    <div class="jumbotron">
        <h2>Gerir Passageiros</h2>                    
        <?= form_open('passageiro/novo') ?>
        <div class="form-group">
            <label  for="nome" >Nome categoria de  passageiro</label>
            <input type="text" class="form-control" name="categoria" id="categoria"  required>
        </div>
        <div class="form-group">
            <label  for="cobranca">Valor a pagar:</label>
            <select class="form-control" name="cobranca" id="cobranca" required>
                <option value="">Selecione uma opção</option>
                <option value="2">Integral</option>
                <option value="1">Metade</option>
                <option value="0">Isento</option>              
            </select>
        </div>
        <button class="btn btn-primary">Salvar</button>
        <?= form_close(); ?>

        <br><br>
        <a class="btn btn-danger btn-lg" href="<?php echo site_url('passageiro/ver') ?>" role="button">Ver categorias cadastradas</a>
        <br><br>
    </div>
</div>
