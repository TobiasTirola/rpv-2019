<div class="container">
    <h1>Gerenciamento de Trajetos</h1>
    <?= form_open('trajetocont/visualizar') ?>
    <div class="form-group">
        <label for="linha_id">Trajeto</label>
        <select class="form-control" name="linha_id" id="linha_id" required>
            <option value="">Selecione uma opção</option>
            <?php foreach ($trajetos as $trajeto) : ?>
                <option value="<?= $trajeto['num_trajeto'] ?>"><?= $trajeto['nome_trajeto'] ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <button class="btn btn-success">Visualizar</button>
    <?= form_close(); ?>
<!--    <table class="table">
        <tr>
            <th>Id</th>
            <th>Nome</th>
            <th>Paradas</th>
            <th>Horário</th>
        </tr>

        //<?php foreach ($trajetos as $trajeto) : ?>
                <tr>
                    <td>//<?= $trajeto['num_trajeto'] ?></td>
                    <td>//<?= $trajeto['nome_trajeto'] ?></td>
                    //<?php foreach ($paradas as $parada): ?>
                        //<?php print_r($parada) ?>
                            //<?php if ($parada[id_parada] == $trajeto['id_parada']): ?>
                                    //<?php print_r($parada['id_parada']) ?>
                                    //<?php print_r($trajeto) ?>
                                    <td>//<?= $parada['local'] ?></td>
                                //<?php endif; ?>
                        //<?php endforeach; ?>
                    <td>//<?= $trajeto['horariochegada'] ?></td>
                </tr>
            //<?php endforeach ?>
    </table>-->
    <br>
    <form action="trajeto_view.php" method="post">
        <h2>Adicionar Trajetos</h2>
        <?php
        echo form_open($action = "trajetocont/novo");
//        echo form_label($label_text = 'Nome do Trajeto:', $id = 'nome');
//        echo form_input(array(
//            "name" => "nome",
//            "id" => "nome",
//            "class" => "form-control",
//            "maxlength" => "255"));

//        echo form_label($label_text = 'Cidade:', $id = 'cidade');
//        echo form_input(array(
//            "name" => "cidade",
//            "id" => "cidade",
//            "class" => "form-control",
//            "maxlength" => "255"));
//        ?>
        <div class="form-group">
            <label  for="nome" >Nome do Trajeto</label>
            <input type="text" class="form-control" name="nome" id="nome" required>
        </div>
        <div class="form-group">
            <label for="cidade" >Cidade</label>
            <input type="text" class="form-control" name="cidade" id="cidade"  required>
        </div>
       
        <h5><b>Paradas:</b></h5>
        <?php
        $dados = array(
            'name' => 'nome',
            'id' => 'nome',
            'estilo' => 'margem: 10px'
        );
        ?>
        
        


        <?php foreach ($paradas as $parada) : ?>
            <div class="checkbox">
                <label>
                    <?php
                    echo form_checkbox("paradas[]", "Parada 1", set_checkbox("paradas[]", "Parada 1"));
                    ?> <?= $parada['id'] ?>, <?= $parada['local'] ?>, <?= $parada['referencia'] ?>
                </label>
            </div>
        <?php endforeach ?>
        
         <a href="<?php echo site_url('trajetocont/novo') ?>" type="submit" class="btn btn-sucess"> Salvar</a>
         <?= form_close(); ?>
    </form>
</div>
