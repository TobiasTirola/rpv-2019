<!-- Begin Page Content -->
<div class="container-fluid">
    <form action="<?php echo base_url('cliente/buscaPassagemEspecifica') ?>" method="post">
        <label>Local De Saída:  </label><input type="text" id="localSaida" name="localSaida">
        <label>Local De Destino:    </label><input type="text" id="localDestino" name="localDestino">
        <label>Data:    </label><input type="date" id="data" name="data">
        <button class="btn btn-primary" type="submit">
            <i class="fas fa-search fa-sm"></i>
        </button>
    </form>
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"></h1>

        <table class="table">

            <tr>
                <th>Nome da linha</th>
            </tr>
            <?php foreach ($linhas as $linha): ?>
                <tr>
                    <td><?= $linha['nome'] ?></td>
                    <td>
                        <form action="<?php echo base_url('cliente/compraPassagem') ?>" method="post"><input type="hidden"
                                                                        value="<?php echo $linha['idLinhaOnibus'] ?>" id="id" name="id">
                            <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">Visualizar</button>
                        </form>
                    </td>

                </tr>
            <?php endforeach ?>
        </table>
    </div>


</div>
<!-- /.container-fluid -->


