<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class   Cliente extends CI_Controller
{


    public function index()
    {
        $this->load->view('templates/headerCliente');
        $this->load->view('templates/navCliente');
        $this->load->view('cliente/cliente_view');
        $this->load->view('templates/footerCliente');
        $this->load->view('templates/js');
    }

    public function buscaPassagem()
    {
        $this->load->model("linha_model");
        $lista = $this->linha_model->buscaTodos();
        $dados = array("linhas" => $lista);
        $this->load->view('templates/headerCliente', $dados);
        $this->load->view('templates/navCliente');
        $this->load->view('cliente/busca_passagem_view');
        $this->load->view('templates/footerCliente');
        $this->load->view('templates/js');
    }

    public function compraPassagem()
    {
        $id = $this->input->post("id");
        $this->load->model("linha_model");
        $linha = $this->linha_model->retorna($id);
        $this->load->model("trecho_model");
        $lista = $this->trecho_model->retornaPorLinha($id);
        $dados = array("linha" => $linha,
            "trechos" => $lista);
        $this->load->view('templates/headerCliente', $dados);
        $this->load->view('templates/navCliente');
        $this->load->view('cliente/compra_passagem_view');
        $this->load->view('templates/footerCliente');
        $this->load->view('templates/js');
    }

    public function buscaPassagemEspecifica()
    {
        $origem = $this->input->post("localSaida");
        $destino = $this->input->post("localDestino");
        $data = $this->input->post("data");
        $this->load->model("trecho_model");
        $diasemana = date('w', strtotime($data));
        $lista = $this->trecho_model->buscaEspecifica($origem, $destino, $diasemana);
        $dados = array("trechos" => $lista);
        if($lista){
            $this->load->view('templates/headerCliente', $dados);
            $this->load->view('templates/navCliente');
            $this->load->view('cliente/busca_passagem_especifica_view');
            $this->load->view('templates/footerCliente');
            $this->load->view('templates/js');
        }else{

            ?>
            <script type="text/javascript">
                alert("Não foram encontradas passagens para sua busca");
            </script>
            <?php
            redirect("cliente/buscaPassagem");

        }

    }

}
