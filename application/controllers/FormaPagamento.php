<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class FormaPagamento extends CI_Controller {
    


    public function ver() {
        $this->load->model("formaPagamento_model");
        $dado = $this->formaPagamento_model->buscaTodos();
        $dados = array('formas' => $dado);

        $this->load->view('templates/headerAdm');
        $this->load->view('templates/navAdm');
        $this->load->view('pages/formas', $dados);
        $this->load->view('templates/footerAdm');
        $this->load->view('templates/js');
    }

    public function novo() {
        $this->ver();
        $formapagamento = array(
            "FORMAPAGAMENTO" => $this->input->post("formapagamento"),
            "DESCRICAO" => $this->input->post("descricao")
        );
        $this->load->model('formaPagamento_model');
        $this->formaPagamento_model->salva($formapagamento);
        $this->load->view('templates/headerAdm');
        $this->load->view('templates/navAdm');
        $this->load->view('pages/trajetosUrbanos');
        $this->load->view('templates/footerAdm');
        $this->load->view('templates/js');
    }

}
