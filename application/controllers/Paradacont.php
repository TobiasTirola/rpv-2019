<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Paradacont extends CI_Controller {

    public function index() {
        $this->load->model("parada_model");
        $lista = $this->parada_model->buscaTodos();
        $dados = array("parada" => $lista);
        $this->load->view('templates/headerAdm');
        $this->load->view('templates/navAdm');        
        $this->load->view('pages/parada_view', $dados);
        $this->load->view('templates/footerAdm');
        $this->load->view('templates/js');        
        
    }

    public function novo() {
        $parada = array(
            "local" => $this->input->post("local"),
            "referencia" => $this->input->post("referencia")
        );
        $this->load->model("parada_model");
        $this->parada_model->salva($parada);
        $this->session->set_flashdata("success", "Parada Adicionada!");
        redirect('/');
    }

}
