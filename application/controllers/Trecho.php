<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class   Trecho extends CI_Controller {


	public function index()
	{
        $this->load->view('templates/headerAdm');
        $this->load->view('templates/navAdm');
        $this->load->view('administrador/cadastro_trecho_view');
        $this->load->view('templates/footerAdm');
        $this->load->view('templates/js');
	}
	public function cadastra()
    {
        $this->load->view('templates/headerAdm');
        $this->load->view('templates/navAdm');
        $this->load->view('administrador/cadastro_trecho_view');
        $this->load->view('templates/footerAdm');
        $this->load->view('templates/js');
    }
    public function busca(){
        $this->load->model("linha_model");
        $lista = $this->linha_model->buscaTodos();
        $dados = array("linha"=>$lista);
        $this->load->view('templates/headerAdm', $dados);
        $this->load->view('templates/navAdm');
        $this->load->view('administrador/busca_trecho_view');
        $this->load->view('templates/footerAdm');
        $this->load->view('templates/js');
    }
        public function novo(){
            $trecho=array(
                "inicio" => $this->input->post("inicio"),
                "fim" => $this->input->post("fim"),
                "distanciaLinha" => $this->input->post("distanciaLinha"),
                "trajeto" => $this->input->post("trajeto")
            );
            $this->load->model("trecho_model");
            $this->trecho_model->salva($trecho);
            ?>
            <script type="text/javascript">
                alert("Cadastrado com sucesso");
            </script>
            <?php
        }
}
