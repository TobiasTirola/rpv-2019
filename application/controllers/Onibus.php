<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class   Onibus extends CI_Controller
{


    public function index()
    {
        $this->load->view('templates/headerSecretario');
        $this->load->view('templates/navSecret');
        $this->load->view('secretario/cadastro_onibus_view');
        $this->load->view('templates/footerSecretario');
        $this->load->view('templates/js');
    }

    public function cadastra()
    {
        $this->load->view('templates/headerSecretario');
        $this->load->view('templates/navSecret');
        $this->load->view('secretario/cadastro_onibus_view');
        $this->load->view('templates/footerSecretario');
        $this->load->view('templates/js');
    }

    public function busca()
    {
        $this->load->model("onibus_model");
        $lista = $this->onibus_model->buscaTodos();
        $dados = array("onibus" => $lista);
        $this->load->view('templates/headerSecretario', $dados);
        $this->load->view('templates/navSecret');
        $this->load->view('secretario/busca_onibus_view');
        $this->load->view('templates/footerSecretario');
        $this->load->view('templates/js');
    }

    public function novo()
    {

        $onibus = array(
            "dataDeFabricacao" => $this->input->post("dtfabricacao"),
            "marca" => $this->input->post("marca"),
            "modelo" => $this->input->post("modelo"),
            "dataAquisicao" => $this->input->post("dtAqui"),
            "quilometragem" => $this->input->post("quilometragem"),
            "tipoonibus" => $this->input->post("tipoOnibus"),
            "placa" => $this->input->post("placa"),
            "renavan" => $this->input->post("renavan"),
            "cor" => $this->input->post("cor"),
            "nmrAssentos" => $this->input->post("nmrAssent"),
            "nmrMaxPassageiros" => $this->input->post("nmrMax"),
            "status" => $this->input->post("status"),

        );

        $this->load->model("onibus_model");
        $this->onibus_model->salva($onibus);
        ?>
        <script type="text/javascript">
            alert("Cadastrado com sucesso");
        </script>
        <?php
    }
}
