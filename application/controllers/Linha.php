<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class   Linha extends CI_Controller
{


    public function index()
    {
        $this->load->model("trecho_model");
        $lista = $this->trecho_model->buscaTodos();
        $dados = array("trechos" => $lista);
        $this->load->view('templates/headerAdm', $dados);
        $this->load->view('templates/navAdm');
        $this->load->view('administrador/cadastro_linha_view', $dados);
        $this->load->view('templates/footerAdm');
        $this->load->view('templates/js');
    }

    public function cadastra()
{
    $this->load->model("trecho_model");
    $lista = $this->trecho_model->buscaTodos();
    $dados = array("trechos" => $lista);
    $this->load->view('templates/headerAdm', $dados);
    $this->load->view('templates/navAdm');
    $this->load->view('administrador/cadastro_linha_view', $dados);
    $this->load->view('templates/footerAdm');
    $this->load->view('templates/js');
}
    public function edita()
    {
        $id = $this->input->post("id");
        $this->load->model("trecho_model");
        $listaTrechos = $this->trecho_model->buscaTodos();
        $this->load->model("linha_model");
        $linha = $this->linha_model->retorna($id);
        $dados = array("trechos" => $listaTrechos,
            "linha" => $linha);
        $this->load->view('templates/headerAdm', $dados);
        $this->load->view('templates/navAdm');
        $this->load->view('administrador/edita_linha_view', $dados);
        $this->load->view('templates/footerAdm');
        $this->load->view('templates/js');
    }
    public function busca()
    {
        $this->load->model("linha_model");
        $lista = $this->linha_model->buscaTodos();
        $dados = array("linhas" => $lista);
        $this->load->view('templates/headerAdm', $dados);
        $this->load->view('templates/navAdm');
        $this->load->view('administrador/busca_linha_view');
        $this->load->view('templates/footerAdm');
        $this->load->view('templates/js');
    }

    public function novo()
    {

        $linha = array(
            "nome" => $this->input->post("nome"),
            "itinerario" => $this->input->post("itinerario"),
            "localSaida" => $this->input->post("localSaida"),
            "localDestino" => $this->input->post("localDestino"));

        $this->load->model("linha_model");
        $this->linha_model->salva($linha);
        ?>
        <div class="alert alert-success">
            <strong>Success!</strong> Indicates a successful or positive action.
        </div>
        <?php
        redirect($uri='/');
    }
    public function edit($id){
        $this->load->model("linha_model");
        $this->linha_model->salvar();
        ?>
        <script type="text/javascript">
            alert("Editado com sucesso");
        </script>
        <?php
        redirect($uri='/');
    }
}
