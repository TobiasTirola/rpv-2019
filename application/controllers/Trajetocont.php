<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Trajetocont extends CI_Controller {

    public function index() {
        $this->load->model("trajeto_model");
        $lista = $this->trajeto_model->buscaTodos();
        $this->load->model("parada_model");
        $lista2 = $this->parada_model->buscaTodos();
        $dados = array("paradas" => $lista2, "trajetos" => $lista);
        $this->load->view('templates/headerAdm');
        $this->load->view('templates/navAdm');
        $this->load->view('trajeto/trajeto_view', $dados);
        $this->load->view('templates/footerAdm');
        $this->load->view('templates/js');
        //$this->load->view('paradas/parada_cad');
    }

    public function novo() {
        $trajeto = array(
            "nome" => $this->input->post("nome"),
            "cidade" => $this->input->post("cidade")
        );
        print_r($trajeto);
        $this->load->model("trajeto_model");
        $this->trajeto_model->salva($trajeto);
        redirect('/');
    }

    public function visualizar() {
        $this->load->view('templates/header');
        $this->load->view('templates/navAdm');
        // $this->load->view('pages/', $concessao);
        $this->load->view('templates/footerAdm');
        $this->load->view('templates/js');
    }

}
