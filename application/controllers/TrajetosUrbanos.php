<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class TrajetosUrbanos extends CI_Controller {

    public function index() {
        $this->load->view('templates/headerAdm');
        $this->load->view('templates/navAdm');
        $this->load->view('pages/trajetosUrbanos');
        $this->load->view('templates/footerAdm');
        $this->load->view('templates/js');
    }

    public function novaTarifa() {
        $tarifa = array(
            "id_linha" => $this->input->post("id"),
            "valor" => $this->input->post("valor"),
            "data" => $this->input->post("data"),
            "leiDecreto" => $this->input->post("lei")
        );
        $this->load->model('alterarTarifa_model');
        $this->alterarTarifa_model->novaTarifa($tarifa);
        $this->session->set_flashdata("sucess", "Tarifa alterada com sucesso");
        redirect('index');
    }

    public function gerirConcessoes() {
        $this->load->view('templates/headerAdm');
        $this->load->view('templates/navAdm');
        $this->load->model('alterarTarifa_model');
        $lista = $this->alterarTarifa_model->buscarLinha();
        $listaEmpresas = $this->alterarTarifa_model->buscarEmpresa();
        $dados = array('linhas' => $lista, 'empresas' => $listaEmpresas);
        $this->load->view('pages/gerirConcessoes', $dados);
        $this->load->view('templates/footerAdm');
        $this->load->view('templates/js');
    }

    public function gerirTrajetos() {
        $this->load->view('templates/headerAdm');
        $this->load->view('templates/navAdm');
        $this->load->model("trajeto_model");
        $lista = $this->trajeto_model->buscaTodos();
        $this->load->model("parada_model");
        $lista2 = $this->parada_model->buscaTodos();
        $dados = array("paradas" => $lista2, "trajetos" => $lista);
        $this->load->view('pages/trajeto_view', $dados);
        //    $this->load->view('pages/parada_view',$dados);

        $this->load->view('templates/footerAdm');
        $this->load->view('templates/js');
    }

    public function alocarfunc() {
        $this->load->view('templates/headerAdm');
        $this->load->view('templates/navAdm');
        $this->load->view('pages/alocarFunc');
        $this->load->view('templates/footerAdm');
        $this->load->view('templates/js');
    }

    public function gerirPassageiros() {
        $this->load->model("passageiro_model");
        $dado = $this->passageiro_model->buscar();
        $dados = array('categorias' => $dado);
        $this->load->view('templates/headerAdm');
        $this->load->view('templates/navAdm');
        $this->load->view('pages/catPassageiros', $dados);
        $this->load->view('pages/gerirPassageiros');
        $this->load->view('templates/footerAdm');
        $this->load->view('templates/js');
    }

    public function formasPagamento() {
        $this->load->model("formaPagamento_model");
        $dado = $this->formaPagamento_model->buscaTodos();
        $dados = array('formas' => $dado);
        $this->load->view('templates/headerAdm');
        $this->load->view('templates/navAdm');
        $this->load->view('pages/formas', $dados);
        $this->load->view('pages/formaDePagamentoCadastro');
        $this->load->view('templates/footerAdm');
        $this->load->view('templates/js');
    }

    public function programaPontos() {
        $this->load->view('templates/headerAdm');
        $this->load->view('templates/navAdm');
        $this->load->view('pages/pontos');
        $this->load->view('templates/footerAdm');
        $this->load->view('templates/js');
    }

}
