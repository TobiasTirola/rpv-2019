<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Concessoes extends CI_Controller {

    public function index() {
        
    }

    public function editar($id) {
        $this->load->model('concessao_model');
        $dado = $this->concessao_model->buscar($id);
        $concessao = array('concessao' => $dado);
        $this->load->view('templates/headerAdm');
        $this->load->view('templates/navAdm');
        $this->load->view('pages/editarConcessao', $concessao);
        $this->load->view('templates/footerAdm');
        $this->load->view('templates/js');
    }

    public function salva() {
        $this->load->model("concessao_model");
        $this->concessao_model->update();
        $this->load->view('templates/headerAdm');
        $this->load->view('templates/navAdm');
        $this->load->view('pages/gerirConcessoes');
        $this->load->view('templates/footerAdm');
        $this->load->view('templates/js');
    }

    public function visualizar() {
        $id = $this->input->post("linha_id");
        $this->load->model("concessao_model");
        $dado = $this->concessao_model->buscar($id);
        $concessao = array('concessao' => $dado, 'id' => $id);
        $this->load->view('templates/headerAdm');
        $this->load->view('templates/navAdm');
        $this->load->view('pages/detalheConcessao', $concessao);
        $this->load->view('templates/footerAdm');
        $this->load->view('templates/js');
    }

    public function novo() {
        $concessao = array(
            'contrato' => $this->input->post('contrato'),
            'dataVigencia' => $this->input->post('data'),
            'status' => $this->input->post('status'),
            'id_trajeto' => $this->input->post('id_trajeto')
        ); 
        $this->load->model('concessao_model');
        $this->concessao_model->salvar($concessao);
        $this->load->view('templates/headerAdm');
        $this->load->view('templates/navAdm');
        $this->load->view('pages/trajetosUrbanos');
        $this->load->view('templates/footerAdm');
        $this->load->view('templates/js');
    }

    public function cadastrarNova($id) {
        $this->load->model('trajeto_model');
        $dado = $this->trajeto_model->buscar($id);
        $trajeto = array('trajeto' => $dado);
        $this->load->view('templates/headerAdm');
        $this->load->view('templates/navAdm');
        $this->load->view('pages/cadastrarConcessao', $trajeto);
        $this->load->view('templates/footerAdm');
        $this->load->view('templates/js');
    }

}
