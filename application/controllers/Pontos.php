<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pontos extends CI_Controller {

    public function resgatar($cpf) {
        $this->load->model('passageiro_model');
        $pontos = $this->passageiro_model->buscarPontos($cpf);
        $cliente = $this->passageiro_model->buscarDadosCliente($cpf);
        $dados = array('pontos' => $pontos, 'cliente' => $cliente);
        $this->load->view('templates/headerAdm');
        $this->load->view('templates/navAdm');
        $this->load->view('pages/resgatePontos', $dados);
        $this->load->view('templates/footerAdm');
        $this->load->view('templates/js');
    }

}
