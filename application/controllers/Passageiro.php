<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Passageiro extends CI_Controller {

    public function novo() {
        $passageiro = array(
            'descricao' => $this->input->post('categoria'),
            'valorCobrado' => $this->input->post('cobranca')
        );
        $this->load->model('passageiro_model');
        $this->passageiro_model->novo($passageiro);
        $this->load->view('templates/headerAdm');
        $this->load->view('templates/navAdm');
        $this->load->view('pages/gerirPassageiros');
        $this->load->view('templates/footerAdm');
        $this->load->view('templates/js');
    }

    public function ver() {
        $this->load->model("passageiro_model");
        $dado = $this->passageiro_model->buscar();
        $dados = array('categorias' => $dado);
        $this->load->view('templates/headerAdm');
        $this->load->view('templates/navAdm');
        $this->load->view('pages/catPassageiros', $dados);
        $this->load->view('templates/footerAdm');
        $this->load->view('templates/js');
    }

    public function consultarPontos() {
        $cpf = $this->input->post('cpf');
        $this->load->model('passageiro_model');
        $pontos = $this->passageiro_model->buscarPontos($cpf);
        $cliente = $this->passageiro_model->buscarDadosCliente($cpf);
        $dados  = array('pontos' => $pontos, 'cliente' => $cliente);
        $this->load->view('templates/headerAdm');
        $this->load->view('templates/navAdm');
        $this->load->view('pages/verPontos', $dados);
        $this->load->view('templates/footerAdm');
        $this->load->view('templates/js');
    }

}
