<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tarifa extends CI_Controller {


	public function index()
	{
                $this->load->model("tarifa_model");
		        $lista = $this->tarifa_model->buscaTodos();
                $dados = array("tarifa"=>$lista);
                $this->load->view('admlocal/cadastra_tarifa_view', $dados);
	}
	public function cadastra()
    {
        $this->load->view('admLocal/cadastra_tarifa_view');
    }
        public function novo(){
            
            $tarifa=array(
               "valor" => $this->input->post("valor"),
                "categoria" => $this->input->post("categoria"),
               "data" => $this->input->post("data"),



            );
            
            $this->load->model("tarifa_model");
            $this->tarifa_model->salva($tarifa);
              ?>
            <script type="text/javascript">
                alert("Cadastrado com sucesso");
            </script>
    <?php

        }
}
