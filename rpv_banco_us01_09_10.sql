-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 15-Maio-2019 às 01:15
-- Versão do servidor: 10.1.38-MariaDB
-- versão do PHP: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rpv`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `linhaonibus`
--

CREATE TABLE `linhaonibus` (
  `idLinhaOnibus` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `itinerario` text,
  `localSaida` varchar(45) DEFAULT NULL,
  `localDestino` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `linhaonibus`
--

INSERT INTO `linhaonibus` (`idLinhaOnibus`, `nome`, `itinerario`, `localSaida`, `localDestino`) VALUES
(2, 'Linha Alegrete x Porto Alegre', 'Sai da rodoviária de alegrete, pega a br 145 e segue direto até porto alegre', 'Alegrete', 'Porto Alegre'),
(3, 'Linha 1', 'Passa pela br 123 e vai direto até a cidade de Buenos aires', 'Alegrete', 'Buenos aires'),
(4, 'aushsyag', 'yagsgsayg', 'yagsyags', 'ygaysgas'),
(5, 'Linha teste 2', 'Sai de tal lugar, vai por tal lugar e chega no destino', 'Alegrete', 'Santa Catarina'),
(6, 'Linha teste 3', 'chega em tal lugar por tal lugar', 'Alegrete', 'São paulo'),
(7, '', '', '', ''),
(8, 'Linha Alegrete x Porto Alegre', 'Sai da rodoviária de alegrete, pega a br 145 e segue direto até porto alegre', 'Alegrete', 'Porto Alegre'),
(9, 'Linha Alegrete x Porto Alegre', 'Sai da rodoviária de alegrete, pega a br 145 e segue direto até porto alegre', 'Alegrete', 'Porto Alegre'),
(10, 'Linha Alegrete x Porto Alegre', 'Sai da rodoviária de alegrete, pega a br 145 e segue direto até porto alegre', 'Alegrete', 'Porto Alegre'),
(11, 'Linha Alegrete x Porto Alegre', 'Sai da rodoviária de alegrete, pega a br 145 e segue direto até porto alegre', 'Alegrete', 'Porto Alegre');

-- --------------------------------------------------------

--
-- Estrutura da tabela `onibus`
--

CREATE TABLE `onibus` (
  `idOnibus` int(11) NOT NULL,
  `dataDeFabricacao` varchar(45) DEFAULT NULL,
  `marca` varchar(45) DEFAULT NULL,
  `modelo` varchar(45) DEFAULT NULL,
  `dataAquisicao` varchar(45) DEFAULT NULL,
  `quilometragem` double DEFAULT NULL,
  `tipoOnibus` varchar(45) DEFAULT NULL,
  `placa` varchar(45) DEFAULT NULL,
  `renavan` int(45) DEFAULT NULL,
  `cor` varchar(45) DEFAULT NULL,
  `nmrAssentos` int(11) DEFAULT NULL,
  `nmrMaxPassageiros` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `onibus`
--

INSERT INTO `onibus` (`idOnibus`, `dataDeFabricacao`, `marca`, `modelo`, `dataAquisicao`, `quilometragem`, `tipoOnibus`, `placa`, `renavan`, `cor`, `nmrAssentos`, `nmrMaxPassageiros`, `status`) VALUES
(1, '03/01/2010', 'Mercedes Bens', 'LO 815', '05/08/2018', 364, 'Interurbano', 'KOJ-2054', 21545552, 'Azul', 45, 45, 1),
(2, '03/01/2010', 'Mercedes Bens', 'LO 815', '05/08/2018', 364, 'Interurbano', 'KLJ-2648', 3333333, 'Azul', 45, 45, 1),
(3, '03/01/2010', 'Mercedes Bens', 'LO 815', '05/08/2018', 364, 'Interurbano', 'WKH-3645', 54848484, 'Azul', 45, 45, 1),
(4, '03/01/2010', 'Mercedes Bens', 'LO 815', '05/08/2018', 364, 'Interurbano', 'UBA-1254', 54848484, 'Azul', 45, 45, 1),
(5, '03/01/2010', 'Mercedes Bens', 'LO 815', '05/08/2018', 364, 'Interurbano', 'GBH-1574', 54848484, 'Azul', 45, 45, 1),
(6, '03/01/2010', 'Mercedes Bens', 'LO 815', '05/08/2018', 364, 'Interurbano', 'KHQ-9874', 54848484, 'Azul', 45, 45, 1),
(7, '03/01/2010', 'Mercedes Bens', 'LO 815', '05/08/2018', 364, 'Interurbano', 'NBA-3648', 54848484, 'Azul', 45, 45, 1),
(8, '2019-12-29', '15', 'UKA58', '2019-12-31', 5156, 'Urbano', 'KKK-3333', 6666666, 'Azul', NULL, NULL, 0),
(9, '2019-12-29', '15', 'UKA58', '2019-12-31', 5156, 'Urbano', 'JJJ-6666', 11111111, 'Azul', NULL, NULL, 0),
(10, '2019-12-31', 'Mercedes', 'UKA58', '2019-12-31', 5156, 'Interurbano', 'AHS-8055', 154848, 'Azul', NULL, NULL, 0),
(11, '2016-05-14', 'Mercedes', 'UKA58', '2019-05-14', 580, 'Urbano', 'JJJ-6666', 56055626, 'Azul', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tarifa`
--

CREATE TABLE `tarifa` (
  `idTarifa` int(11) NOT NULL,
  `valor` double NOT NULL,
  `categoria` int(11) NOT NULL,
  `data` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tarifa`
--

INSERT INTO `tarifa` (`idTarifa`, `valor`, `categoria`, `data`) VALUES
(1, 350, 0, '2019-12-31'),
(2, 0, 0, ''),
(3, 0, 0, ''),
(4, 350, 1, ''),
(5, 2, 3, '2121-03-22'),
(6, 12, 1, '2019-12-31'),
(7, 12, 3, '2019-12-31');

-- --------------------------------------------------------

--
-- Estrutura da tabela `trecho`
--

CREATE TABLE `trecho` (
  `idTrecho` int(11) NOT NULL,
  `inicio` varchar(45) NOT NULL,
  `fim` varchar(45) NOT NULL,
  `distanciaLinha` double NOT NULL,
  `trajeto` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `trecho`
--

INSERT INTO `trecho` (`idTrecho`, `inicio`, `fim`, `distanciaLinha`, `trajeto`) VALUES
(1, 'Alegrete', 'Porto Alegre', 300, 'br125'),
(2, 'Alegrete', 'Uruguaiana', 120, 'BR125');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `linhaonibus`
--
ALTER TABLE `linhaonibus`
  ADD PRIMARY KEY (`idLinhaOnibus`);

--
-- Indexes for table `onibus`
--
ALTER TABLE `onibus`
  ADD PRIMARY KEY (`idOnibus`);

--
-- Indexes for table `tarifa`
--
ALTER TABLE `tarifa`
  ADD PRIMARY KEY (`idTarifa`);

--
-- Indexes for table `trecho`
--
ALTER TABLE `trecho`
  ADD PRIMARY KEY (`idTrecho`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `linhaonibus`
--
ALTER TABLE `linhaonibus`
  MODIFY `idLinhaOnibus` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `onibus`
--
ALTER TABLE `onibus`
  MODIFY `idOnibus` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tarifa`
--
ALTER TABLE `tarifa`
  MODIFY `idTarifa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `trecho`
--
ALTER TABLE `trecho`
  MODIFY `idTrecho` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
